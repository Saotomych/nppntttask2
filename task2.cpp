/*
* Project: False coin searcher AKA Task2
*
* Author: Alex Alyoshkin
* Date: 03.02.2015
* Libs: STL
*/

/* Task 
* Some bank received information from reliable sources that in their last group of N coins exactly one coin is false and differs in weight from other coins (while all other coins are equal in weight). After the economic crisis they have only a simple balance available (with two balanced pans). Using this balance, one is able to determine if the weight of objects in the left pan is less than, greater than, or equal to the weight of objects in the right pan.
* In order to detect the false coin the bank employees numbered all coins by the integers from 1 to N, thus assigning each coin a unique integer identifier. After that they began to weight various groups of coins by placing equal numbers of coins in the left pan and in the right pan. The identifiers of coins and the results of the weightings were carefully recorded.
* You are to write a program that will help the bank employees to determine the identifier of the false coin using the results of these weightings.
* Input
* The first line of the input file contains two integers N and K, separated by spaces, where N is the number of coins () and K is the number of weightings fulfilled (). The following 2K lines describe all weightings. Two consecutive lines describe each weighting. The first of them starts with a number Pi (), representing the number of coins placed in the left and in the right pans, followed by Pi identifiers of coins placed in the left pan and Pi identifiers of coins placed in the right pan. All numbers are separated by spaces. The second line contains one of the following characters: '<', '>', or '='. It represents the result of the weighting:
* '<' means that the weight of coins in the left pan is less than the weight of coins in the right pan,
* '>' means that the weight of coins in the left pan is greater than the weight of coins in the right pan,
* '=' means that the weight of coins in the left pan is equal to the weight of coins in the right pan.
*
* Output
* Write to the output file the identifier of the false coin or 0, if it cannot be found by the results of the given weightings.
*/

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <memory>
#include <fstream>
#include <sstream>
#include <iostream>
#include <bitset>

// Definition module
#define MAXN	100

typedef unsigned int DWORD;
typedef unsigned char BYTE;

using namespace std;

// Diagnose module
class IDiagnose
{
public:
   virtual void ExitFailure(DWORD strnum) { cerr << endl << "String " << strnum << ": Unexpected symbol detected "; }
   virtual void ExitEOF(DWORD strnum) { cerr << endl << "String "<< strnum << ": Unexpected EOF detected ";}
};

class CQCoinsDiagnose: public IDiagnose
{
   string PrintPlace() { return "when maximum quantity of coins was waiting"; }
public:
   virtual void ExitFailure(DWORD strnum) { IDiagnose::ExitFailure(strnum); cerr << PrintPlace() << endl; }
   virtual void ExitEOF(DWORD strnum) {IDiagnose::ExitEOF(strnum); cerr << PrintPlace() <<  endl; }
};

class CWCoinsDiagnose: public IDiagnose
{
   string PrintPlace() { return "when quantity of weightings was waiting"; }
public:
   virtual void ExitFailure(DWORD strnum) { IDiagnose::ExitFailure(strnum); cerr << PrintPlace() << endl; }
   virtual void ExitEOF(DWORD strnum) { IDiagnose::ExitEOF(strnum); cerr << PrintPlace() << endl; }
};

class CCoinSetDiagnose: public IDiagnose
{
   string PrintPlace() { return "when coin set was waiting"; }
public:
   virtual void ExitFailure(DWORD strnum) {IDiagnose::ExitFailure(strnum); cerr << PrintPlace() << endl; }
   virtual void ExitEOF(DWORD strnum) { IDiagnose::ExitEOF(strnum); cerr << PrintPlace() << endl; }
};

class CExpressionDiagnose: public IDiagnose
{
   string PrintPlace() { return "when expression was waiting"; }
public:
   virtual void ExitFailure(DWORD strnum) { IDiagnose::ExitFailure(strnum); cerr << PrintPlace() << endl; }
   virtual void ExitEOF(DWORD strnum) { IDiagnose::ExitEOF(strnum); cerr << PrintPlace() << endl; }
};

// Executing module

// Return: maxumim coin ID
int ParseMaxCoinsQuant(stringstream& str)
{
  int ret;
  str >> ret;
  if (!ret) throw 23;

  return ret;
}

// Return: quantity of weightings
int ParseMaxWgtQuant(stringstream& str)
{
   int ret;
   str >> ret;
  if (!ret) throw 23;

   return ret;
}

// Creates bitmask of existing numbers from stream
// Return: How many digits were in string
int CreateBitSetByNumbers(stringstream& str, bitset<MAXN>& bSetLeft, bitset<MAXN>& bSetRight, BYTE iMaxCoins)
{
   int cnt;
  
   str >> cnt;

   if (!cnt) throw 23;

   int n;
   for (DWORD i = 0; i < cnt; ++i)
   {
      str >> n;
      if (!n) throw 23;
      if (n > iMaxCoins) throw 21;

      bSetLeft.set(n-1);
   }

   for (DWORD i = 0; i < cnt; ++i)
   {
      str >> n;
      if (!n) throw 23;
      if (n > iMaxCoins) throw 21;

      bSetRight.set(n-1);
   }

   return 2*cnt;
}

// Takes symbol from stream
// Return: symbol '=' | '<' | '>'
char GetSymbol(stringstream& str)
{
   char ret;
   str >> ret;

   if ( ret >= '0' && ret <='9' ) throw 22;
   if ( ret != '<' && ret != '>' && ret != '=') throw 20;

   return ret;
}

// BitSet tests for alone set bit
// Return: bitnumber or 0
DWORD GetAloneBit1(bitset<MAXN>& bSet, DWORD dwMaxCoins)
{
   DWORD Ret=0;

   bitset<MAXN> b1;
   b1.flip();

   bSet &= (b1 >> MAXN-dwMaxCoins);

   if ((dwMaxCoins-bSet.count()) == 1)
   {
      while (bSet.test(Ret)) ++Ret;
      ++Ret;
   }
   
   return Ret;
}

// Main function
int main(int argc, char *argv[])
{

  // Read file to stringstream buffer
  // stringstream use for easy integer taking
  stringstream strStream;

  {
    auto_ptr<ifstream> hFl(new ifstream("coin.in"));
    if (!hFl->is_open())
    {
	cout << "File coin.in not found" << endl;
        exit (-1);
    }
   
    copy( (istreambuf_iterator<char>(*(hFl).get() )),
        istreambuf_iterator<char>(),
        ostreambuf_iterator<char>(strStream)
        );
  }

  // Exception ON
  strStream.exceptions( ifstream::failbit | ifstream::eofbit );

  // Actual diagnostic messages
  IDiagnose *pDiag = 0;

  // Local definitions
  bitset<MAXN> bsMainCoinMask, bsCoinMaskL, bsCoinMaskH;
  DWORD dwStrNum = 1; // From first text string
  DWORD dwMaxCoins = 0;
 
  try
  {
    // Instance of all diagnose classes made for fast speed only
    auto_ptr<CQCoinsDiagnose> pdiag1(new CQCoinsDiagnose);
    auto_ptr<CWCoinsDiagnose> pdiag2(new CWCoinsDiagnose);
    auto_ptr<CCoinSetDiagnose> pdiag3(new CCoinSetDiagnose);
    auto_ptr<CExpressionDiagnose> pdiag4(new CExpressionDiagnose);
 
    try 
    {    
       pDiag = pdiag1.get();
       dwMaxCoins = ParseMaxCoinsQuant(strStream);

       pDiag = pdiag2.get();
       int iMaxWgts = ParseMaxWgtQuant(strStream);

       ++dwStrNum;

       for (DWORD wts = 0; wts < iMaxWgts; ++wts)
       {

          // Digit string
          pDiag = pdiag3.get();
          
          bitset<MAXN> bsMaskLeft, bsMaskRight;
          CreateBitSetByNumbers(strStream, bsMaskLeft, bsMaskRight, dwMaxCoins);

          ++dwStrNum;

          // Symbol string
          pDiag = pdiag4.get();
          
	  char ch = GetSymbol(strStream);
	  if (ch == '=')
          {
              bsMainCoinMask |= bsMaskLeft | bsMaskRight;	   // All using coins are true
          }
          else
	  {
              bsMainCoinMask |= ~(bsMaskLeft | bsMaskRight);   // All not using coins are true

   	      if (ch == '<') 
              {
                 bsCoinMaskL |= bsMaskLeft;
                 bsCoinMaskH |= bsMaskRight;
              }

   	      if (ch == '>') 
              {
                 bsCoinMaskL |= bsMaskRight;
                 bsCoinMaskH |= bsMaskLeft;
	      }

              bsMainCoinMask |= bsCoinMaskL & bsCoinMaskH;
          }

          ++dwStrNum;

       }

       // Get result
       int Res = GetAloneBit1(bsMainCoinMask, dwMaxCoins);

       // Save result to file
       {
          auto_ptr<ofstream> hFl(new ofstream("coin.out"));
          if (hFl->is_open()) *hFl << Res;
          else cerr << "File coin.out not created" << endl;
       }

       dwStrNum = 0;  // exit code = 0

    } 

    catch (ifstream::failure ex)
    {
       if (strStream.eof()) pDiag->ExitEOF(dwStrNum);
       if (strStream.fail()) pDiag->ExitFailure(dwStrNum);
    }
    catch (int e)
    {
       if (e == 22) --dwStrNum;
       pDiag->ExitFailure(dwStrNum);
       if (e == 21) cerr << "Additional information: coin ID found greater then maximum coin ID" << endl;
       if (e == 23) cerr << "Additional information: unexpected 0" << endl;
    }

  }

  catch (bad_alloc& ba)
  {
     cerr << "Bad memory allocation" << endl;
     exit(-1);
  }

  exit(dwStrNum);   // exit code != 0 if was exception

}


