#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
using namespace std;

int main() 
{
    string s;
    getline(cin, s);
    
    stringstream stream;
    stream << s;

    while (!stream.eof())
    {
        uint64_t v;
        stream >> v;
        bool f1, f2;
        if (!(f1 = v%3))
            cout << "foo";
        if (!(f2 = v%5))
            cout << "bar";
        if (f1&&f2)
            cout << to_string(v);
        cout << endl;
    }
    
    return 0;
}
